import React from "react";
import { Link as GatsbyLink } from "gatsby";
import { navigate } from "gatsby-link";

export const Link = (props) => {
  const { to, state, onClick, replace, ...rest } = props;

  return (
    <GatsbyLink
      to={to}
      state={state}
      onClick={(e) => {
        if (onClick) {
          onClick(e);
        }

        if (
          e.button === 0 && // ignore right clicks
          !props.target && // let browser handle "target=_blank"
          !e.defaultPrevented && // onClick prevented default
          !e.metaKey && // ignore clicks with modifier keys...
          !e.altKey &&
          !e.ctrlKey &&
          !e.shiftKey
        ) {
          e.preventDefault();

          if (typeof document !== "undefined") {
            let spinner = document.getElementById("gatsby-plugin-spinner");
            spinner.style.webkitTransition = "none";
            spinner.style.mozTransition = "none";
            spinner.style.msTransition = "none";
            spinner.style.oTransition = "none";
            spinner.style.transition = "none";
            spinner.style.opacity = 1;
          }

          // Make sure the necessary scripts and data are
          // loaded before continuing.
          navigate(to, { state, replace });
        }

        return true;
      }}
      {...rest}
    />
  );
};
