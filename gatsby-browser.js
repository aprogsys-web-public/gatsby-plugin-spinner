import "css-spinners/dist/all.css";

const defaultOptions = { theme: "primary", top: 0, left: 0 };

export const onClientEntry = (a, pluginOptions = {}) => {
  const options = { ...defaultOptions, ...pluginOptions };

  const node = document.createElement(`div`);
  node.id = `gatsby-plugin-spinner`;
  node.style.zIndex = 99999999999;
  node.style.position = "fixed";
  node.style.top = options.top;
  node.style.left = options.left;
  node.className = "spin " + options.theme;
  document.body.appendChild(node);
};

export const onPreRouteUpdate = () => {
  let spinner = document.getElementById("gatsby-plugin-spinner");
  spinner.style.webkitTransition = "none";
  spinner.style.mozTransition = "none";
  spinner.style.msTransition = "none";
  spinner.style.oTransition = "none";
  spinner.style.transition = "none";
  spinner.style.opacity = 1;
};

export const onRouteUpdate = () => {
  let spinner = document.getElementById("gatsby-plugin-spinner");
  spinner.style.webkitTransition = "opacity 1s ease-in-out";
  spinner.style.mozTransition = "opacity 1s ease-in-out";
  spinner.style.msTransition = "opacity 1s ease-in-out";
  spinner.style.oTransition = "opacity 1s ease-in-out";
  spinner.style.transition = "opacity 1s ease-in-out";
  spinner.style.opacity = 0;
};
